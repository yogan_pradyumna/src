package pradyumna.sportskpi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {

    EditText uname, pass;
    Button signIn;
    TextView errorMsg;

    int flag = 0;

    CharSequence text = "" ;
    Context context;
    Toast toast;
    int duration = Toast.LENGTH_SHORT;

    Boolean firstRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("LOGIN");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);


        Typeface font = Typeface.createFromAsset(Login.this.getAssets(),
                "fonts/fabiolo.ttf");


        firstRun = Boolean.FALSE;
        uname = (EditText) findViewById(R.id.uname);
        pass = (EditText) findViewById(R.id.password);
        errorMsg = (TextView) findViewById(R.id.errorMsg);
        signIn = (Button) findViewById(R.id.btnSignIn);
        signIn.setTypeface(font);

        checkFirstRun();
        if(firstRun.equals(Boolean.FALSE)){

            LoginDBHandler db = new LoginDBHandler(Login.this);
            final LoginItem loginItem = db.getLogin();
            uname.setText(loginItem.getUsername());
            pass.setText(loginItem.getPassword());

            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent("pradyumna.sportskpi.MainActivity");
                    intent.putExtra("club",loginItem.getUsername());
                    startActivity(intent);
                }
            });

        }
        else {

            final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference("clubs");

            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (uname.getText().toString().equals("") || pass.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Please Enter The Credentials", Toast.LENGTH_LONG).show();

                    } else {

                        mRootRef.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                                String unameStr = dataSnapshot.getKey().toString();
                                String passStr = dataSnapshot.getValue(String.class);

                                Log.d("uname", unameStr);

                                if (unameStr.equals(uname.getText().toString()) && passStr.equals(pass.getText().toString())) {

                                    flag = 1;
                                    errorMsg.setText("");
                                    text = "Logging on!";
                                    context = getApplicationContext();
                                    toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                    Log.d("uname", unameStr);
                                    errorMsg.setText("");
                                    if (firstRun.equals(Boolean.TRUE)) {
                                        LoginDBHandler db = new LoginDBHandler(Login.this);
                                        LoginItem loginItem = new LoginItem();
                                        loginItem.setUsername(unameStr);
                                        loginItem.setPassword(passStr);
                                        db.addLogin(loginItem);
                                    }
                                    errorMsg.setText("");
                                    Intent intent = new Intent("pradyumna.sportskpi.MainActivity");
                                    intent.putExtra("club", unameStr);
                                    startActivity(intent);

                                }


                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                    if (flag != 1) {

                        errorMsg.setText("Please check your Credentials and Try again!");

                    }


                }
            });


        }

    }



    //Actions to do when the app is launched for the first time
    public void checkFirstRun() {

        final String PREFS_NAME = "MyPrefsFile";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;


        // Get current version code
        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return;
        }

        // Get saved version code
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            // This is just a normal run
            return;

        } else if (savedVersionCode == DOESNT_EXIST) {

            // TODO This is a new install (or the user cleared the shared preferences)

            firstRun = Boolean.TRUE;


        } else if (currentVersionCode > savedVersionCode) {

            // TODO This is an upgrade

        }

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).commit();

    }

}
