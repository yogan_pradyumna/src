package pradyumna.sportskpi;

/**
 * Created by Pradyumna1 on 4/14/2017.
 */
public class Injury {

    private String playerName;
    private String injury;
    private String intensity;
    private String physioName;
    private String status;
    private String treatmentComments;
    private String injuryDate;
    private String recoveryDate;
    private String medReportUrl;
    private String reasonForInjury;

    public Injury() {
    }

    public Injury(String playerName, String injury, String intensity, String physioName, String status, String treatmentComments, String injuryDate) {
        this.playerName = playerName;
        this.injury = injury;
        this.intensity = intensity;
        this.physioName = physioName;
        this.status = status;
        this.treatmentComments = treatmentComments;
        this.injuryDate = injuryDate;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getInjury() {
        return injury;
    }

    public void setInjury(String injury) {
        this.injury = injury;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public String getPhysioName() {
        return physioName;
    }

    public void setPhysioName(String physioName) {
        this.physioName = physioName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTreatmentComments() {
        return treatmentComments;
    }

    public void setTreatmentComments(String treatmentComments) {
        this.treatmentComments = treatmentComments;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getInjuryDate() {
        return injuryDate;
    }

    public void setInjuryDate(String injuryDate) {
        this.injuryDate = injuryDate;
    }

    public String getRecoveryDate() {
        return recoveryDate;
    }

    public void setRecoveryDate(String recoveryDate) {
        this.recoveryDate = recoveryDate;
    }

    public String getMedReportUrl() {
        return medReportUrl;
    }

    public void setMedReportUrl(String medReportUrl) {
        this.medReportUrl = medReportUrl;
    }

    public String getReasonForInjury() {
        return reasonForInjury;
    }

    public void setReasonForInjury(String reasonForInjury) {
        this.reasonForInjury = reasonForInjury;
    }
}
