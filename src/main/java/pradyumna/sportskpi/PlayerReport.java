package pradyumna.sportskpi;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PlayerReport extends AppCompatActivity {

    private ListView mListView;
    private ArrayList<TreatmentItem> mArrayList = new ArrayList<>();
    private ArrayList<String> mKeys = new ArrayList<>();

    InjuryReportItemAdapter injuryReportItemAdapter;
    PlayerReport CustomListView;

    String player_name, club, injury_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("PLAYER REPORT");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        mListView = (ListView)findViewById(R.id.listViewInjuries);

        Bundle bundle = getIntent().getExtras();
        player_name = bundle.getString("player_name");
        club = bundle.getString("club");

        CustomListView = this;
        Resources res =getResources();
        /**************** Create Custom Adapter *********/
        injuryReportItemAdapter =new InjuryReportItemAdapter( CustomListView, mArrayList, res,this, club);
        mListView.setAdapter(injuryReportItemAdapter);


        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
        DatabaseReference mPlayer = mRootRef.child("players").child(player_name);

        DatabaseReference mInjuries = mPlayer.child("injuries");

        mInjuries.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String key = dataSnapshot.getKey().toString();
                String injuryDate = dataSnapshot.child("injuryDate").getValue().toString();
                String injury = dataSnapshot.child("injury").getValue().toString();
                String intensity = dataSnapshot.child("intensity").getValue().toString();
                String physio = dataSnapshot.child("physioName").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String treatment = dataSnapshot.child("treatmentComments").getValue().toString();

                injury_name = injury;

                if (status.equals("Recovered")) {

                    TreatmentItem treatmentItem = new TreatmentItem();

                    treatmentItem.setTreatmentName(injury);

                    String string = injuryDate.substring(3,5);
                    String month = getMonthForInt(Integer.parseInt(string)-1);

                    treatmentItem.setTreatmentDate(injuryDate.substring(0,2)+" "+month.substring(0,3));
                    treatmentItem.setTreatmentDaysOld("");

                    String curDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
                    String startDate = injuryDate;
                    SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                    String inputString1 = startDate.replace('/', ' ');
                    String inputString2 = curDate.replace('/', ' ');

                    Log.d("start date:", inputString1);
                    Log.d("cur date:", inputString2);

                    try {
                        Date date1 = myFormat.parse(inputString1);

                        Date date2 = myFormat.parse(inputString2);
                        long diff = date2.getTime() - date1.getTime();
                        System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                        Long val = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                        treatmentItem.setTreatmentDaysOld(val.toString());

                    } catch (ParseException e) {
                        e.printStackTrace();
                        treatmentItem.setTreatmentDaysOld("");
                    }


                    treatmentItem.setPlayerName(player_name);
                    treatmentItem.setInjuryKey(key);
                    treatmentItem.setInjuryName(injury);

                    mArrayList.add(treatmentItem);
                    injuryReportItemAdapter.notifyDataSetChanged();

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent("pradyumna.sportskpi.PlayerReportInjury");
                intent.putExtra("player_name", player_name);
                intent.putExtra("club", club);
                intent.putExtra("injury_name",injury_name);

                TreatmentItem treatmentItem = mArrayList.get(position);
                intent.putExtra("injury_key",treatmentItem.getInjuryKey());

                startActivity(intent);

            }
        });


    }



    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }

}
