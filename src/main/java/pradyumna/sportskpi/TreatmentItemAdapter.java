package pradyumna.sportskpi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Pradyumna1 on 6/12/2017.
 */
public class TreatmentItemAdapter extends BaseAdapter implements View.OnClickListener{

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ArrayList<TreatmentItem> listValues;
    TreatmentItem tempValues=null;
    private Context context;
    String club;

    /*************  CustomAdapter Constructor *****************/
    public TreatmentItemAdapter(Activity a, ArrayList d,Resources resLocal,Context context, String club) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;
        this.context=context;
        this.club = club;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<=0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView treatmentName;
        public TextView treatmentDate;
        public TextView treatmentDaysRemaining;


    }


    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View vi = convertView;
        final ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.treatment_list_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/
            holder = new ViewHolder();
            holder.treatmentName = (TextView) vi.findViewById(R.id.treatmentNameTv);
            holder.treatmentDate = (TextView) vi.findViewById(R.id.tvTreatmentDate);
            holder.treatmentDaysRemaining = (TextView) vi.findViewById(R.id.daysOld);

            Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/fabiolo.ttf");
            holder.treatmentName.setTypeface(face, Typeface.BOLD);
            holder.treatmentDate.setTypeface(face);
            holder.treatmentDaysRemaining.setTypeface(face, Typeface.BOLD);


            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);


        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        if(data.size()<=0)
        {
            holder.treatmentName.setText("");
            holder.treatmentDate.setText("");
            holder.treatmentDaysRemaining.setText("");


        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( TreatmentItem ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.treatmentName.setText(tempValues.getTreatmentName());
            holder.treatmentDate.setText(tempValues.getTreatmentDate());
            if(tempValues.getTreatmentDaysOld().equals("1")) {
                holder.treatmentDaysRemaining.setText(tempValues.getTreatmentDaysOld()+ " Day Ago");
            }else if(tempValues.getTreatmentDaysOld().equals("0")){
                holder.treatmentDaysRemaining.setText(" Today ");
            }
            else{
                holder.treatmentDaysRemaining.setText(tempValues.getTreatmentDaysOld()+ " Days Ago");
            }


            holder.treatmentName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tempValues = (TreatmentItem)data.get(position);

                    Intent intent = new Intent("pradyumna.sportskpi.UpdateTreatmentDetails");
                    intent.putExtra("player_name", tempValues.getPlayerName());
                    intent.putExtra("club", club);
                    intent.putExtra("injury_key",tempValues.getInjuryKey());

                    context.startActivity(intent);


                }
            });

            holder.treatmentDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tempValues = (TreatmentItem)data.get(position);

                    Intent intent = new Intent("pradyumna.sportskpi.UpdateTreatmentDetails");
                    intent.putExtra("player_name", tempValues.getPlayerName());
                    intent.putExtra("club", club);
                    intent.putExtra("injury_key",tempValues.getInjuryKey());


                    context.startActivity(intent);
                }
            });

            holder.treatmentDaysRemaining.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tempValues = (TreatmentItem)data.get(position);

                    Intent intent = new Intent("pradyumna.sportskpi.UpdateTreatmentDetails");
                    intent.putExtra("player_name", tempValues.getPlayerName());
                    intent.putExtra("club", club);
                    intent.putExtra("injury_key",tempValues.getInjuryKey());


                    context.startActivity(intent);
                }
            });



        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }
}
