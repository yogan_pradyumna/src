package pradyumna.sportskpi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditProfile extends AppCompatActivity {


    private EditText inputName, inputAge, inputPosition, inputCountry;
    private TextInputLayout inputLayoutName, inputLayoutAge, inputLayoutPosition,  inputLayoutCountry;
    private Button save;

    Integer flag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("EDIT PLAYER");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutAge = (TextInputLayout) findViewById(R.id.input_layout_age);
        inputLayoutPosition = (TextInputLayout) findViewById(R.id.input_layout_position);
        inputLayoutCountry = (TextInputLayout) findViewById(R.id.input_layout_country);


        inputName = (EditText) findViewById(R.id.input_name);
        inputAge = (EditText) findViewById(R.id.input_age);
        inputPosition = (EditText) findViewById(R.id.input_position);
        inputCountry = (EditText) findViewById(R.id.input_country);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/fabiolo.ttf");
        inputName.setTypeface(face);
        inputAge.setTypeface(face);
        inputPosition.setTypeface(face);
        inputCountry.setTypeface(face);


        save = (Button) findViewById(R.id.btn_register);

        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputAge.addTextChangedListener(new MyTextWatcher(inputAge));
        inputPosition.addTextChangedListener(new MyTextWatcher(inputPosition));
        inputCountry.addTextChangedListener(new MyTextWatcher(inputCountry));


        Bundle bundle = getIntent().getExtras();
        final String player_name = bundle.getString("player_name");
        final String club = bundle.getString("club");

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);



        mRootRef.child("players").child(player_name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null) {
                    String name = dataSnapshot.child("name").getValue().toString();
                    String age = dataSnapshot.child("age").getValue().toString();
                    String position = dataSnapshot.child("position").getValue().toString();
                    String country = dataSnapshot.child("country").getValue().toString();

                    inputName.setText(name);
                    inputAge.setText(age);
                    inputPosition.setText(position);
                    inputCountry.setText(country);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitForm();

                if (flag == 1) {
                    Toast.makeText(getApplicationContext(), "Please enter the required details", Toast.LENGTH_LONG).show();
                } else {

                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(club);
                    DatabaseReference mPlayer = mDatabase.child("players");

                    Player player = new Player();
                    player.setName(inputName.getText().toString());
                    player.setAge(inputAge.getText().toString());
                    player.setCountry(inputCountry.getText().toString());
                    player.setPosition(inputPosition.getText().toString());

                    //Storing values to firebase
                    //ref.child("feedback").setValue(person);

                    // Creating new user node, which returns the unique key value
                    //String id = mDatabase.push().getKey();
                    // new user node would be /users/$userid/

                    // pushing user to 'users' node using the userId

                    mPlayer.child(player_name).removeValue();
                    mPlayer.child(inputName.getText().toString()).setValue(player);

                    Context context = getApplicationContext();
                    CharSequence text = "Player Details have been updated!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    Intent intent = new Intent(EditProfile.this, MainActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("club", club);
                    startActivity(intent);
                    finish();

                }

            }
        });

    }


    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateName()) {
            flag=1;
        }

        if (!validateAge()) {
            flag=1;
        }

        if (!validatePosition()) {
            flag=1;
        }

        if (!validateCountry()) {
            flag=1;
        }

    }

    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError("Please Enter The Name");
            requestFocus(inputName);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAge() {
        if (inputAge.getText().toString().trim().isEmpty()) {
            inputLayoutAge.setError("Please Enter The Age");
            requestFocus(inputAge);
            return false;
        } else {
            inputLayoutAge.setErrorEnabled(false);
        }

        return true;


    }

    private boolean validatePosition() {

        if (inputPosition.getText().toString().trim().isEmpty()) {
            inputLayoutPosition.setError("Please Enter The Position");
            requestFocus(inputPosition);
            return false;
        } else {
            inputLayoutPosition.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCountry() {

        if (inputCountry.getText().toString().trim().isEmpty()) {
            inputLayoutCountry.setError("Please Enter The Country");
            requestFocus(inputCountry);
            return false;
        } else {
            inputLayoutCountry.setErrorEnabled(false);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_age:
                    validateAge();
                    break;
                case R.id.input_position:
                    validatePosition();
                    break;
                case R.id.input_country:
                    validateCountry();
                    break;

            }
        }
    }

    public class Player {
        //name and address string
        private String name;
        private String age;
        private String position;
        private String country;

        public Player() {
        }

        public Player(String name, String age, String position, String country) {
            this.name = name;
            this.age = age;
            this.position = position;
            this.country = country;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

}

