package pradyumna.sportskpi;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class PlayerReportInjury extends AppCompatActivity {


    TextView playerName, injuryName, daysToRecover, timeSpent, painRange, rt1, rt2, rt3, treatmentReportXLS, medReport;
    ImageView downloadXLS, downloadMedReport;

    String player_name, club, injuryKey, injuryname;
    String downloadUrl;

    String timespent, painrange;
    Integer tspent = 0;
    ArrayList<Integer> prange= new ArrayList<>();

    // declare the dialog as a member field of your activity
    ProgressDialog mProgressDialog;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"
    };

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    int id = 0;




    HSSFWorkbook workbook;
    Map<String, Object[]> data;
    HSSFSheet sheet;
    Integer count;

    ProgressDialog progress;
    Integer thrd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_report_injury);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progress = ProgressDialog.show(this, "Loading Data... Please Wait !",
                "Trying to establish a network connection ", true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(6000);
                }

                catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }

                progress.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(thrd==0) {
                            // Toast.makeText(Cse_activity.this, "Not able connect ", Toast.LENGTH_SHORT).show();
                            Toast msg = Toast.makeText(PlayerReportInjury.this, "Not able to Connect ! Please check your Internet connection", Toast.LENGTH_LONG);

                            msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);

                            msg.show();
                        }

                    }
                });
            }
        }).start();

        SpannableString s = new SpannableString("INJURY REPORT");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        verifyStoragePermissions(PlayerReportInjury.this);

        Typeface font = Typeface.createFromAsset(PlayerReportInjury.this.getAssets(),
                "fonts/fabiolo.ttf");

        rt1 = (TextView)findViewById(R.id.rt1);
        rt2 = (TextView)findViewById(R.id.rt2);
        rt3 = (TextView)findViewById(R.id.rt3);
        rt1.setTypeface(font, Typeface.BOLD);
        rt2.setTypeface(font, Typeface.BOLD);
        rt3.setTypeface(font, Typeface.BOLD);

        playerName = (TextView)findViewById(R.id.playerNameReport);
        playerName.setTypeface(font, Typeface.BOLD);

        injuryName = (TextView)findViewById(R.id.injuryNameReport);
        injuryName.setTypeface(font, Typeface.BOLD);

        daysToRecover = (TextView)findViewById(R.id.daysToRecover);
        timeSpent = (TextView)findViewById(R.id.timeSpent);
        painRange = (TextView)findViewById(R.id.painRange);

        treatmentReportXLS = (TextView)findViewById(R.id.treatmentReportXls);
        treatmentReportXLS.setTypeface(font, Typeface.BOLD);
        medReport = (TextView)findViewById(R.id.medReportDownload);
        medReport.setTypeface(font, Typeface.BOLD);

        downloadXLS = (ImageView)findViewById(R.id.imageViewXLS);
        downloadMedReport = (ImageView)findViewById(R.id.imageViewMedReport);

        Bundle bundle = getIntent().getExtras();
        player_name = bundle.getString("player_name");
        club = bundle.getString("club");
        injuryKey = bundle.getString("injury_key");
        injuryname = bundle.getString("injury_name");

        playerName.setText("NAME: " + player_name);

        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Treatment Report");
        count=3;
        data = new HashMap<String, Object[]>();


        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
        DatabaseReference mPlayer = mRootRef.child("players").child(player_name);
        DatabaseReference mInjuries = mPlayer.child("injuries");

        mInjuries.child(injuryKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String injuryDate = dataSnapshot.child("injuryDate").getValue().toString();
                String injury = dataSnapshot.child("injury").getValue().toString();

                String intensity = dataSnapshot.child("intensity").getValue().toString();
                String physio = dataSnapshot.child("physioName").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String treatment = dataSnapshot.child("treatmentComments").getValue().toString();
                String recoveryDate = dataSnapshot.child("recoveryDate").getValue().toString();
                downloadUrl = dataSnapshot.child("medReportUrl").getValue().toString();

                injuryName.setText("INJURY: "+ injury);

                String curDate = recoveryDate;
                String startDate = injuryDate;
                SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                String inputString1 = startDate.replace('/', ' ');
                String inputString2 = curDate.replace('/', ' ');

                Log.d("start date:", inputString1);
                Log.d("cur date:", inputString2);

                try {
                    Date date1 = myFormat.parse(inputString1);

                    Date date2 = myFormat.parse(inputString2);
                    long diff = date2.getTime() - date1.getTime();
                    System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                    Long val = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                    daysToRecover.setText(val.toString());

                } catch (ParseException e) {
                    e.printStackTrace();
                    daysToRecover.setText(" - ");
                }

                timeSpent.setText("20");
                painRange.setText("10-2");

                thrd=1;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        data.put("1", new Object[]{"Injury: "+injuryname});
        data.put("2", new Object[]{"TREATMENT DATE", "TREATMENT TIME", "TREATMENT DETAILS", "TIME SPENT", "PAIN LEVEL",  "STATUS"});
        mInjuries.child(injuryKey).child("treatments").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String date = dataSnapshot.child("dateOfTreatment").getValue(String.class);
                String time = dataSnapshot.child("timeOfTreatment").getValue(String.class);
                String treatmentDetails = dataSnapshot.child("treatmentDetails").getValue(String.class);
                String painLevel = dataSnapshot.child("painLevel").getValue(String.class);
                String status = dataSnapshot.child("status").getValue(String.class);
                String totaltime = dataSnapshot.child("timeSpent").getValue(String.class);

                prange.add(Integer.parseInt(painLevel));
                painRange.setText(Collections.max(prange)+"-"+Collections.min(prange));
                tspent+= Integer.parseInt(totaltime);
                timeSpent.setText(tspent.toString());

                String[] str = new String[11];

                str[0] = date;
                str[1] = time;
                str[2] = treatmentDetails;
                str[3] = totaltime;
                str[4] = painLevel;
                str[5] = status;

                data.put(count.toString(), new Object[]{str[0], str[1], str[2], str[3], str[4], str[5]});
                count++;

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        downloadMedReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String p = player_name.replace(' ', '_');
                String i = injuryname.replace('/', '_');

                String pf = p.replace('.','_');

                Intent intent = new Intent();
                Uri pathUri = Uri.parse("/sdcard/" + pf + "_" + "MedicalReport" + i);
                intent.setDataAndType(pathUri, "image/*");

                final PendingIntent contentIntent = PendingIntent.getActivity(PlayerReportInjury.this, (int) System.currentTimeMillis(), intent, 0);
                mBuilder = new NotificationCompat.Builder(PlayerReportInjury.this);

                mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder = new NotificationCompat.Builder(PlayerReportInjury.this);
                mBuilder.setContentTitle("SportsKPI: Player Medical Report")
                        .setContentText("Download in progress")
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.medical_report);


// instantiate it within the onCreate method
                mProgressDialog = new ProgressDialog(PlayerReportInjury.this);
                mProgressDialog.setMessage("Downloading Medical Report...");
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(true);

// execute this when the downloader must be fired
                final DownloadTask downloadTask = new DownloadTask(PlayerReportInjury.this);

                downloadTask.execute(downloadUrl);

                mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        downloadTask.cancel(true);
                    }
                });

            }
        });


        downloadXLS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Set<String> keyset = data.keySet();
                int rownum = 1;
                for (Integer key=1;key<count;key++) {

                    Log.d("key:", key.toString());

                    Row row = sheet.createRow(rownum++);
                    Object [] objArr = data.get(key.toString());
                    int cellnum = 0;
                    for (Object obj : objArr) {
                        Cell cell = row.createCell(cellnum++);

                        Log.d("obj",obj.toString());
                        if(obj instanceof Date)
                            cell.setCellValue((Date)obj);
                        else if(obj instanceof Boolean)
                            cell.setCellValue((Boolean)obj);
                        else if(obj instanceof String)
                            cell.setCellValue((String)obj);
                        else if(obj instanceof Double)
                            cell.setCellValue((Double)obj);
                    }
                }

                String p = player_name.replace(' ', '_');
                String i = injuryname.replace('/', '_');
                String FILE = Environment.getExternalStorageDirectory()
                        + "/" +p+"_"+i+"_Treatment_Report.xls";

                try {



                    FileOutputStream out =
                            new FileOutputStream(FILE);
                    workbook.write(out);
                    out.close();
                    System.out.println("Excel written successfully..");

                    Context context = getApplicationContext();
                    CharSequence text = "File Successfully Downloaded!";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File file = new File(Environment.getExternalStorageDirectory(),
                        "/" +p+"_"+i+"_Treatment_Report.xls");
                Uri path = Uri.fromFile(file);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/xls");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                try {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e) {
                    Toast.makeText(PlayerReportInjury.this,
                            "No Application Available to View PDF",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });




    }

    // usually, subclasses of AsyncTask are declared inside the activity class.
// that way, you can easily modify the UI thread from here
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                String type = connection.getContentType();
                Log.d("Type:",type);
                String ext = type.substring(type.lastIndexOf("/")+ 1,type.length());
                Log.d("Ext",ext);

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                /*Uri uri = Uri.parse(url.toString());
                fileType = new String(getMimeType(uri));
                Log.d("Mimetype:",fileType);*/

                String p = player_name.replace(' ', '_');
                String i = injuryname.replace('/', '_');

                String pf = p.replace('.', '_');

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream("/sdcard/"+pf+"_"+"MedicalReport_"+ i +"."+ext);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }

            int i;
            for (i = 0; i <= 100; i += 5) {
                // Sets the progress indicator completion percentage
                publishProgress(Math.min(i, 100));
                try {
                    // Sleep for 5 seconds
                    Thread.sleep(2 * 1000);
                } catch (InterruptedException e) {
                    Log.d("TAG", "sleep failure");
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();

            mBuilder.setProgress(100, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);

            mBuilder.setProgress(100, progress[0], false);
            mNotifyManager.notify(id, mBuilder.build());
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();


            }
            else
                Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();

            mBuilder.setContentText("Download complete");
            // Removes the progress bar
            mBuilder.setProgress(0, 0, false);
            mNotifyManager.notify(id, mBuilder.build());

        }



    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, "android.permission.WRITE_EXTERNAL_STORAGE");

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


}
