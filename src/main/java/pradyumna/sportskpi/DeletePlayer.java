package pradyumna.sportskpi;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class DeletePlayer extends AppCompatActivity {

    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

    private ListView mListView;
    private ArrayList<String> mArrayList = new ArrayList<>();
    PlayerDeleteAdapter playerDeleteAdapter;
    DeletePlayer CustomListView;

    String club;

    ProgressDialog progress;
    Integer thrd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("DELETE PLAYER");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        mListView = (ListView)findViewById(R.id.listViewPlayers);

        Bundle bundle = getIntent().getExtras();
        club = bundle.getString("club");


        CustomListView = this;
        Resources res =getResources();
        /**************** Create Custom Adapter *********/
        playerDeleteAdapter =new PlayerDeleteAdapter( CustomListView, mArrayList, res,this, club);
        mListView.setAdapter(playerDeleteAdapter);

        progress = ProgressDialog.show(this, "Loading Data... Please Wait !",
                "Trying to establish a network connection ", true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(6000);
                }

                catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }

                progress.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(thrd==0) {
                            // Toast.makeText(Cse_activity.this, "Not able connect ", Toast.LENGTH_SHORT).show();
                            Toast msg = Toast.makeText(DeletePlayer.this, "Not able to Connect ! Please check your Internet connection", Toast.LENGTH_LONG);

                            msg.setGravity(Gravity.CENTER, msg.getXOffset() / 2, msg.getYOffset() / 2);

                            msg.show();
                        }
                    }
                });
            }
        }).start();



    }

    @Override
    public void onResume(){

        super.onResume();

        mArrayList.clear();

        mRootRef.child(club).child("players").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String name = dataSnapshot.getKey().toString();

                mArrayList.add(name);

                playerDeleteAdapter.notifyDataSetChanged();

                thrd = 1;
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}
