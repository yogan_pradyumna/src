package pradyumna.sportskpi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AddInjury extends AppCompatActivity implements OnItemSelectedListener {


    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

    Spinner spinner, spinnerInjury, spinnerIntensity, spinnerStatus;

    EditText physio, comments;
    TextView medReport;
    Button add;
    ImageView medUploadIcon;

    private static final int FILE_SELECT_CODE = 0;
    int PICK_IMAGE_REQUEST = 111;
    Uri filePath;
    String dUrl;
    ProgressDialog pd;

    String club;
    String playerName;

    String otherInjury, otherReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_injury);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("ADD INJURY");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        physio = (EditText)findViewById(R.id.editTextPhysio);
        add = (Button)findViewById(R.id.btnAddInjury);

        comments = (EditText)findViewById(R.id.editTextComments);
        medReport = (TextView)findViewById(R.id.medReport);

        medUploadIcon = (ImageView)findViewById(R.id.medReportIcon);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/fabiolo.ttf");
        comments.setTypeface(face);
        medReport.setTypeface(face);
        physio.setTypeface(face);


        pd = new ProgressDialog(this);
        pd.setMessage("Uploading....");

        Bundle bundle = getIntent().getExtras();
         club = bundle.getString("club");
         playerName = bundle.getString("player_name");

        // Spinner element
         spinner = (Spinner) findViewById(R.id.spinner);
        spinnerInjury = (Spinner) findViewById(R.id.spinner2);
        spinnerIntensity = (Spinner) findViewById(R.id.spinner3);
        spinnerStatus = (Spinner) findViewById(R.id.spinner4);

        spinner.setPrompt("Player Name");

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        spinnerIntensity.setOnItemSelectedListener(this);
        spinnerInjury.setOnItemSelectedListener(this);
        spinnerStatus.setOnItemSelectedListener(this);


        // Spinner Drop down elements
        final List<String> players = new ArrayList<String>();

        if (!playerName.equals("")){

            players.add(playerName);
        }
        else {
            players.add("Player Name");
        }
        final List<String> injuries = new ArrayList<String>();
        injuries.add("Injury/Illness");
        injuries.add("Cold/Flu/Fever");
        injuries.add("Dislocation");
        injuries.add("Fracture");
        injuries.add("Inflammation/Swelling");
        injuries.add("Respiratory");
        injuries.add("Sprain/Tear");
        injuries.add("Others");

        final List<String> intensity = new ArrayList<String>();
        intensity.add("Intensity");
        intensity.add("Grade 1");
        intensity.add("Grade 2");
        intensity.add("Grade 3");

        final List<String> status = new ArrayList<String>();
        status.add("Reason for Injury");
        status.add("Collision with Object/Player");
        status.add("Awkward Landing");
        status.add("Jump");
        status.add("Over Exertion");
        status.add("Overuse");
        status.add("Fall/Trip/Slip");
        status.add("Other Reasons");


        mRootRef.child(club).child("players").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String name = dataSnapshot.getKey().toString();

                players.add(name);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        // Creating adapter for spinner
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                this,
                R.layout.spinner_item,
                players
        );

        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(adapter);

        // Creating adapter for spinner
        MySpinnerAdapter adapter1 = new MySpinnerAdapter(
                this,
                R.layout.spinner_item,
                injuries
        );

        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerInjury.setAdapter(adapter1);

        // Creating adapter for spinner
        MySpinnerAdapter adapter2 = new MySpinnerAdapter(
                this,
                R.layout.spinner_item,
                intensity
        );
        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerIntensity.setAdapter(adapter2);

        // Creating adapter for spinner
        MySpinnerAdapter adapter3 = new MySpinnerAdapter(
                this,
                R.layout.spinner_item,
                status
        );
        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerStatus.setAdapter(adapter3);



        medReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(
                            Intent.createChooser(intent, "Select a File to Upload"),
                            FILE_SELECT_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    // Potentially direct the user to the Market with a Dialog
                    Toast.makeText(AddInjury.this, "Please install a File Manager.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        medUploadIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(
                            Intent.createChooser(intent, "Select a File to Upload"),
                            FILE_SELECT_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    // Potentially direct the user to the Market with a Dialog
                    Toast.makeText(AddInjury.this, "Please install a File Manager.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
                DatabaseReference mDatabase = mRootRef.child("players");

                Injury injury = new Injury();

                injury.setPlayerName(spinner.getSelectedItem().toString());
                if(otherInjury == null) {
                    injury.setInjury(spinnerInjury.getSelectedItem().toString());
                }
                else {
                    injury.setInjury(otherInjury);
                }

                injury.setIntensity(spinnerIntensity.getSelectedItem().toString());
                injury.setPhysioName(physio.getText().toString());

                if(otherReason == null) {
                    injury.setReasonForInjury(spinnerStatus.getSelectedItem().toString());
                }
                else {
                    injury.setReasonForInjury(otherReason);
                }

                injury.setStatus("Yet To Recover");

                if(!comments.getText().toString().equals("")){
                injury.setTreatmentComments(comments.getText().toString());
                }
                else {
                    injury.setTreatmentComments("None");
                }
                if(dUrl!= null && !dUrl.equals("")) {
                    injury.setMedReportUrl(dUrl);
                }
                else
                injury.setMedReportUrl("//");


                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                injury.setInjuryDate(dateFormat.format(date));

                DatabaseReference mPlayer = mDatabase.child(injury.getPlayerName());
                DatabaseReference mInjury = mPlayer.child("injuries");

                String id = mInjury.push().getKey();
                // pushing user to 'users' node using the userId
                mInjury.child(id).setValue(injury);

                Context context = getApplicationContext();
                CharSequence text = "Injury has been recorded!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                finish();

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        if(item.equals("Others")){

            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(this);
            View promptsView = li.inflate(R.layout.other_injury, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set prompts.xml to alertdialog builder
            alertDialogBuilder.setView(promptsView);

            final EditText injuryN = (EditText)promptsView.findViewById(R.id.editTextOther);


            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // get user input and set it to result

                                otherInjury = injuryN.getText().toString();
                                    Log.d("otherInjury",otherInjury);


                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

        if(item.equals("Other Reasons")){

            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(this);
            View promptsView = li.inflate(R.layout.other_reason, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set prompts.xml to alertdialog builder
            alertDialogBuilder.setView(promptsView);

            final EditText reasonN = (EditText)promptsView.findViewById(R.id.editTextOtherReason);



            // set dialog message
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // get user input and set it to result

                                    otherReason = reasonN.getText().toString();
                                    Log.d("otherReason",otherReason);


                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.img_upload_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final ImageView img = (ImageView) promptsView
                .findViewById(R.id.imgViewUpload);

        final TextView imgName = (TextView)promptsView
                .findViewById(R.id.tvImgName);

        final Button uploadBtn = (Button) promptsView
                .findViewById(R.id.btnUpload);


        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("DONE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result



                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        if (requestCode == FILE_SELECT_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                //getting image from gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting image to ImageView
                img.setImageBitmap(bitmap);

                imgName.setText(filePath.getLastPathSegment());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filePath!=null){
                    pd.show();
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://injury-management-system.appspot.com");

                    StorageReference riversRef = storageRef.child(club + "/" + playerName + "/" + filePath.getLastPathSegment());

                    UploadTask uploadTask = riversRef.putFile(filePath);
                    Log.d("filepath: ", "" + filePath);

                    // Register observers to listen for when the download is done or if it fails
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            Log.d("uploadFail", "" + exception);

                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            //sendNotification("upload backup", 1);
                            pd.dismiss();
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            Log.d("MainActivity", downloadUrl.toString());
                            Toast.makeText(AddInjury.this, "Upload successful", Toast.LENGTH_SHORT).show();
                            dUrl = downloadUrl.toString();

                            medReport.setText("Upload Successful");

                            //Uri downloadUrl = taskSnapshot.getDownloadUrl();

                            Log.d("downloadUrl", "" + downloadUrl);
                        }
                    });

                }
            }
        });


    }


    private static class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/fabiolo.ttf");

        // (In reality I used a manager which caches the Typeface objects)
        // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }

        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTextSize(19);
            view.setTypeface(font);
            return view;
        }

        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTextColor(getContext().getResources().getColor(R.color.white));
            view.setTextSize(19);
            view.setTypeface(font);
            return view;
        }
    }

}
