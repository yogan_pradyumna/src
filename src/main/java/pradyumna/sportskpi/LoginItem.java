package pradyumna.sportskpi;

/**
 * Created by Pradyumna1 on 6/12/2017.
 */
public class LoginItem {

    private String username;
    private String password;

    public LoginItem() {
    }

    public LoginItem(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
