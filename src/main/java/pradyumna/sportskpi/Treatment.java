package pradyumna.sportskpi;

/**
 * Created by Pradyumna1 on 5/10/2017.
 */
public class Treatment {

    private String status;
    private String dateOfTreatment;
    private String timeOfTreatment;
    private String treatmentDetails;
    private String painLevel;
    private String timeSpent;

    public Treatment() {
    }

    public Treatment(String status, String dateOfTreatment, String treatmentDetails) {
        this.status = status;
        this.dateOfTreatment = dateOfTreatment;
        this.treatmentDetails = treatmentDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateOfTreatment() {
        return dateOfTreatment;
    }

    public void setDateOfTreatment(String dateOfTreatment) {
        this.dateOfTreatment = dateOfTreatment;
    }

    public String getTreatmentDetails() {
        return treatmentDetails;
    }

    public void setTreatmentDetails(String treatmentDetails) {
        this.treatmentDetails = treatmentDetails;
    }

    public String getPainLevel() {
        return painLevel;
    }

    public void setPainLevel(String painLevel) {
        this.painLevel = painLevel;
    }

    public String getTimeOfTreatment() {
        return timeOfTreatment;
    }

    public void setTimeOfTreatment(String timeOfTreatment) {
        this.timeOfTreatment = timeOfTreatment;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }
}
