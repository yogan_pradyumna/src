package pradyumna.sportskpi;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PlayerProfile extends AppCompatActivity {

    TextView playerName, playerAge, playerPosition, playerCountry;

    Button viewReport, addInjury, updateTreatment, updatePlayer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("PLAYER PROFILE");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);

        playerName = (TextView) findViewById(R.id.playerName);
        playerAge = (TextView) findViewById(R.id.playerAge);
        playerPosition = (TextView) findViewById(R.id.playerPosition);
        playerCountry = (TextView)findViewById(R.id.playerCountry);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/fabiolo.ttf");
        playerName.setTypeface(face, Typeface.BOLD);
        playerAge.setTypeface(face, Typeface.BOLD);
        playerPosition.setTypeface(face, Typeface.BOLD);
        playerCountry.setTypeface(face, Typeface.BOLD);


        addInjury = (Button)findViewById(R.id.btnAddInj);
        updateTreatment = (Button)findViewById(R.id.btnUpdTreat);
        viewReport = (Button)findViewById(R.id.btnViewReport);
        updatePlayer = (Button)findViewById(R.id.btnUpdplayer);


    }

    @Override
    public void onResume(){
        super.onResume();

        Bundle bundle = getIntent().getExtras();
        final String player_name = bundle.getString("player_name");
        final String club = bundle.getString("club");

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
        DatabaseReference mPlayer = mRootRef.child("players");

        mRootRef.child("players").child(player_name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue()!=null) {
                    String name = dataSnapshot.child("name").getValue().toString();
                    String age = dataSnapshot.child("age").getValue().toString();
                    String position = dataSnapshot.child("position").getValue().toString();
                    String country = dataSnapshot.child("country").getValue().toString();

                    playerName.setText(name);
                    playerAge.setText("Age: " + age);
                    playerPosition.setText("Position: " + position);
                    playerCountry.setText("Country: " + country);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        viewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent("pradyumna.sportskpi.PlayerReport");
                intent.putExtra("player_name", player_name);
                intent.putExtra("club", club);
                startActivity(intent);
            }
        });

        addInjury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("pradyumna.sportskpi.AddInjury");
                intent.putExtra("club",club);
                intent.putExtra("player_name", player_name);
                startActivity(intent);
            }
        });

        updateTreatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent("pradyumna.sportskpi.UpdateTreatment");
                intent.putExtra("player_name", player_name);
                intent.putExtra("club", club);
                startActivity(intent);

            }
        });

        updatePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent("pradyumna.sportskpi.EditProfile");
                intent.putExtra("player_name", player_name);
                intent.putExtra("club", club);
                startActivity(intent);

            }
        });

    }

}
