package pradyumna.sportskpi;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by Pradyumna1 on 6/24/2017.
 */
public class PlayerDeleteAdapter extends BaseAdapter implements View.OnClickListener{

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ArrayList<String> listValues;
    String tempValues=null;
    private Context context;
    String club;

    /*************  CustomAdapter Constructor *****************/
    public PlayerDeleteAdapter(Activity a, ArrayList d,Resources resLocal,Context context, String club) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;
        this.context=context;
        this.club = club;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<=0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView playername;
        public ImageView delIcon;

    }


    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View vi = convertView;
        final ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.player_delete_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/
            holder = new ViewHolder();
            holder.playername = (TextView) vi.findViewById(R.id.treatmentNameTv);

            Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/fabiolo.ttf");
            holder.playername.setTypeface(face);


            holder.delIcon =(ImageView)vi.findViewById(R.id.deleteIcon);


            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);


        }
        else {
            holder = (ViewHolder) vi.getTag();
        }

        if(data.size()<=0)
        {
            holder.playername.setText("");


        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( String ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.playername.setText(tempValues);


            holder.playername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String player = (String) data.get(position);
                    Log.d("Player:", player);

                    final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                            context);

                    // Setting Dialog Title
                    alertDialog2.setTitle("DELETE");

                    // Setting Dialog Message
                    alertDialog2.setMessage("Do you want to delete Player:  " + player + "?");

                    // Setting Positive "Yes" Btn
                    alertDialog2.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog

                                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                    mRootRef.child(club).child("players").child(player).removeValue();

                                    Toast toast = Toast.makeText(context, "Player: " + player + " has been deleted successfully!", Toast.LENGTH_SHORT);
                                    toast.show();

                                    data.remove(position);
                                    notifyDataSetChanged();

                                }
                            });

                    // Setting Negative "NO" Btn
                    alertDialog2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });

                    // Showing Alert Dialog
                    alertDialog2.show();




                }
            });

            holder.delIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String player = (String) data.get(position);
                    Log.d("Player:", player);

                    final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    context);

                    // Setting Dialog Title
                    alertDialog2.setTitle("DELETE");

                    // Setting Dialog Message
                    alertDialog2.setMessage("Do you want to delete Player:  " + player + "?");

                    // Setting Positive "Yes" Btn
                    alertDialog2.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog

                                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                                    mRootRef.child(club).child("players").child(player).removeValue();

                                    Toast toast = Toast.makeText(context, "Player: " + player + " has been deleted successfully!", Toast.LENGTH_SHORT);
                                    toast.show();

                                    data.remove(position);
                                    notifyDataSetChanged();

                                }
                            });

                    // Setting Negative "NO" Btn
                    alertDialog2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });

                    // Showing Alert Dialog
                    alertDialog2.show();


                }
            });



        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }
}
