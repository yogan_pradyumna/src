package pradyumna.sportskpi;

import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UpdateTreatmentDetails extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Spinner status;
    EditText treatmentDetails, timeSpent;
    TextView dateOfTreatment, timeOfTreatment, painHeading;
    Button add;
    DatePicker datePicker;
    TimePicker timePicker;
    SeekBar painLevel;
    String painValue;
    TextView painDisplay;
    ImageView dateChooser, timeChooser;

    String injuryDate;
    String injury;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_treatment_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString("UPDATE TREATMENT DETAILS");
        s.setSpan(new TypefaceSpan(this, "fabiolo.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(s);


        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/fabiolo.ttf");

        status = (Spinner)findViewById(R.id.status);
        dateChooser = (ImageView)findViewById(R.id.dateChooser);
        timeChooser = (ImageView)findViewById(R.id.timeChooser);

        treatmentDetails = (EditText)findViewById(R.id.addTreatment);
        treatmentDetails.setTypeface(face);

        timeSpent = (EditText)findViewById(R.id.durationSpent);
        timeSpent.setTypeface(face);

        dateOfTreatment = (TextView)findViewById(R.id.treatmentDate);
        dateOfTreatment.setTypeface(face);

        timeOfTreatment = (TextView)findViewById(R.id.treatmentTime);
        timeOfTreatment.setTypeface(face);

        add = (Button)findViewById(R.id.btnAddTreatment);

        painHeading = (TextView)findViewById(R.id.textViewPainHeading);
        painLevel = (SeekBar)findViewById(R.id.seekBar);
        painDisplay = (TextView)findViewById(R.id.painDisplay);
        painDisplay.setTypeface(face);
        painHeading.setTypeface(face);

        Bundle bundle = getIntent().getExtras();
        final String player_name = bundle.getString("player_name");
        final String club = bundle.getString("club");
        final String injuryKey = bundle.getString("injury_key");

        status.setPrompt("- - Status - -");
        status.setOnItemSelectedListener(this);

        final List<String> statusTxt = new ArrayList<String>();
        statusTxt.add("Injured");
        statusTxt.add("Good Improvement");
        statusTxt.add("Recovered");

        // Creating adapter for spinner
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                this,
                R.layout.spinner_item,
                statusTxt
        );

        // Drop down layout style - list view with radio button
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // attaching data adapter to spinner
        status.setAdapter(adapter);


        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
        DatabaseReference mPlayer = mRootRef.child("players").child(player_name);

        DatabaseReference mInjuries = mPlayer.child("injuries");

        mInjuries.child(injuryKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                injuryDate = dataSnapshot.child("injuryDate").getValue().toString();
                 injury = dataSnapshot.child("injury").getValue().toString();

                String injuryDetails = new String("\nInjury:" + injury + "\nDate:" + injuryDate + "\n");
                //t.setText(injuryDetails);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        painLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            Integer progressVal = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                Integer val = seekBar.getProgress();
                progressVal = val;
                painValue = new String(val.toString());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                painDisplay.setText(progressVal.toString());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                painDisplay.setText(progressVal.toString());

            }
        });


        dateOfTreatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater li = LayoutInflater.from(UpdateTreatmentDetails.this);
                View promptsView = li.inflate(R.layout.reminder_date, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        UpdateTreatmentDetails.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);


                final int _id = (int) System.currentTimeMillis();
                datePicker = (DatePicker) promptsView.findViewById(R.id.datePicker);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setTitle("Date of Treatment")
                        .setPositiveButton("DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(System.currentTimeMillis());
                                        calendar.set(Calendar.MONTH, datePicker.getMonth());
                                        Integer month = datePicker.getMonth();
                                        month++;
                                        calendar.set(Calendar.YEAR, datePicker.getYear());
                                        calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());

                                        dateOfTreatment.setText(datePicker.getDayOfMonth() + "/" + month.toString() + "/" + datePicker.getYear());

                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        Date injDate = new Date();
                                        Date treatmentDate = new Date();

                                        try {

                                            injDate = formatter.parse(injuryDate);
                                            treatmentDate = formatter.parse(dateOfTreatment.getText().toString());

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        if (injDate.after(treatmentDate)) {

                                            Context context = getApplicationContext();
                                            CharSequence text = "Treatment date can't occur before the Injury Date!";
                                            int duration = Toast.LENGTH_SHORT;

                                            Toast toast = Toast.makeText(context, text, duration);
                                            toast.show();
                                            Log.d("Result", "true");

                                            dateOfTreatment.setText("dd/mm/yyyy");
                                        }

                                    }
                                }

                        )
                        .

                                setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }

                                );

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        dateChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater li = LayoutInflater.from(UpdateTreatmentDetails.this);
                View promptsView = li.inflate(R.layout.reminder_date, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        UpdateTreatmentDetails.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);


                final int _id = (int) System.currentTimeMillis();
                datePicker = (DatePicker) promptsView.findViewById(R.id.datePicker);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setTitle("Date of Treatment")
                        .setPositiveButton("DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(System.currentTimeMillis());
                                        calendar.set(Calendar.MONTH, datePicker.getMonth());
                                        Integer month = datePicker.getMonth();
                                        month++;
                                        calendar.set(Calendar.YEAR, datePicker.getYear());
                                        calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());

                                        dateOfTreatment.setText(datePicker.getDayOfMonth() + "/" + month.toString() + "/" + datePicker.getYear());

                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        Date injDate = new Date();
                                        Date treatmentDate = new Date();

                                        try {

                                            injDate = formatter.parse(injuryDate);
                                            treatmentDate = formatter.parse(dateOfTreatment.getText().toString());

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        if (injDate.after(treatmentDate)) {

                                            Context context = getApplicationContext();
                                            CharSequence text = "Treatment date can't occur before the Injury Date!";
                                            int duration = Toast.LENGTH_SHORT;

                                            Toast toast = Toast.makeText(context, text, duration);
                                            toast.show();
                                            Log.d("Result", "true");

                                            dateOfTreatment.setText("dd/mm/yyyy");
                                        }

                                    }
                                }

                        )
                        .

                                setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }

                                );

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        timeChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater li = LayoutInflater.from(UpdateTreatmentDetails.this);
                View promptsView = li.inflate(R.layout.time_chooser, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        UpdateTreatmentDetails.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);


                final int _id = (int) System.currentTimeMillis();
                timePicker = (TimePicker) promptsView.findViewById(R.id.timePicker);
                timePicker.setIs24HourView(true);
                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setTitle("Time of Treatment")
                        .setPositiveButton("DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(System.currentTimeMillis());

                                        if (Build.VERSION.SDK_INT >= 23 ) {
                                            calendar.set(Calendar.HOUR, timePicker.getHour());
                                            calendar.set(Calendar.MINUTE, timePicker.getMinute());
                                        }
                                            else{
                                            calendar.set(Calendar.HOUR, timePicker.getCurrentHour());
                                            calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                                        }
                                        if (Build.VERSION.SDK_INT >= 23 ){

                                            timeOfTreatment.setText(String.format("%02d", timePicker.getHour()) + ":" + String.format("%02d", timePicker.getMinute()));

                                        }

                                        else{

                                            timeOfTreatment.setText(String.format("%02d", timePicker.getCurrentHour()) + ":" + String.format("%02d", timePicker.getCurrentMinute()));


                                        }


                                    }
                                }

                        )
                        .

                                setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        }

                                );

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate()) {

                    Treatment treatment = new Treatment();
                    treatment.setStatus(status.getSelectedItem().toString());
                    treatment.setDateOfTreatment(dateOfTreatment.getText().toString());
                    treatment.setTimeOfTreatment(timeOfTreatment.getText().toString());
                    treatment.setTreatmentDetails(treatmentDetails.getText().toString());
                    treatment.setPainLevel(painValue);
                    treatment.setTimeSpent(timeSpent.getText().toString());

                    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference(club);
                    DatabaseReference mPlayer = mRootRef.child("players").child(player_name);
                    DatabaseReference mInjuries = mPlayer.child("injuries");
                    DatabaseReference mInjury = mInjuries.child(injuryKey);
                    DatabaseReference mTreatment = mInjury.child("treatments");

                    String id = mTreatment.push().getKey();
                    // pushing user to 'users' node using the userId
                    mTreatment.child(id).setValue(treatment);

                    mInjury.child("status").setValue(treatment.getStatus());

                    if (treatment.getStatus().equals("Recovered")) {
                        mInjury.child("recoveryDate").setValue(dateOfTreatment.getText().toString());
                    }

                    Context context = getApplicationContext();
                    CharSequence text = "Treatment has been recorded!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    finish();
                }

            }
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private static class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/fabiolo.ttf");

        // (In reality I used a manager which caches the Typeface objects)
        // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }

        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(font);
            return view;
        }

        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTextColor(getContext().getResources().getColor(R.color.white));
            view.setTextSize(17);
            view.setTypeface(font);
            return view;
        }
    }

    public Boolean validate(){

        int flag=0;

        if(dateOfTreatment.getText().toString().equals("dd/mm/yyyy")){
            flag=1;
        }else if(timeOfTreatment.getText().toString().equals("hh:mm")){
            flag=1;
        }else if(treatmentDetails.getText().toString().equals("")){
            flag=1;
        }else if(timeSpent.getText().toString().equals("")){
            flag=1;
        }

        if(flag==1){
            Context context = getApplicationContext();
            CharSequence text = "Please enter the details!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return Boolean.FALSE;
        }
        else {
            return Boolean.TRUE;
        }

    }

}
