package pradyumna.sportskpi;

/**
 * Created by Pradyumna1 on 4/7/2017.
 */
public class Player {
    //name and address string
    private String name;
    private String age;
    private String position;
    private String country;

    public Player() {
    }

    public Player(String name, String age, String position, String country) {
        this.name = name;
        this.age = age;
        this.position = position;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
