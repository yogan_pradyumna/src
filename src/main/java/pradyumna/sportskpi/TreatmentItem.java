package pradyumna.sportskpi;

/**
 * Created by Pradyumna1 on 6/29/2017.
 */
public class TreatmentItem {

    private String treatmentName;
    private String treatmentDate;
    private String treatmentDaysOld;
    private String injuryKey;
    private String playerName;
    private String injuryName;


    public TreatmentItem() {
    }

    public TreatmentItem(String treatmentName, String treatmentDate, String treatmentDaysOld) {
        this.treatmentName = treatmentName;
        this.treatmentDate = treatmentDate;
        this.treatmentDaysOld = treatmentDaysOld;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(String treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    public String getTreatmentDaysOld() {
        return treatmentDaysOld;
    }

    public void setTreatmentDaysOld(String treatmentDaysOld) {
        this.treatmentDaysOld = treatmentDaysOld;
    }

    public String getInjuryKey() {
        return injuryKey;
    }

    public void setInjuryKey(String injuryKey) {
        this.injuryKey = injuryKey;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getInjuryName() {
        return injuryName;
    }

    public void setInjuryName(String injuryName) {
        this.injuryName = injuryName;
    }
}
